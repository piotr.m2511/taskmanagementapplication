//class that will pop dialog and ask user for new Profile
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ProfileCreator {

    ProfileScreen parentClass;
    JDialog parent;
    JLabel warningLabel;
    JDialog dialog;

    ProfileCreator(ProfileScreen parentClass){
        this.parentClass = parentClass;
        this.parent = parentClass.profileScreen;
        this.dialog = new JDialog(parent);


        JTextField textField = new JTextField("New profile name");
        dialog.add(textField);

        JButton button = new JButton("Create");
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                if (isNameUnique(textField.getText()))
                dialog.setVisible(false);
            }
        });
        dialog.add(button);

        warningLabel = new JLabel("");
        warningLabel.setForeground(Color.RED);
        dialog.add(warningLabel);

        dialog.setSize(300, 300);
        dialog.setLayout(new FlowLayout(FlowLayout.LEADING));
        dialog.setVisible(true);




    }

    //method creating new profile
    boolean isNameUnique(String profileName){
        //if creating with already used name
        if (parentClass.profileList.contains(profileName)){
            warningLabel.setText("Name already used! Please try another name!");
            return false;
        }
        Profile newProfile = new Profile(profileName);
        parentClass.updateProfileList();
        newProfile = null;
        return true;
    }

}
