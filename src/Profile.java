import java.io.File;
import java.io.IOException;

public class Profile {
    String name;    //username
    TaskList taskList;  //user task list

    String fileName;    //JSON filename containg user's task list
    JsonFileHandler fileHandler;    //handler for JSON files

    //constructor creating filename using username
    public Profile(String name) {
        this.name = name;
        this.taskList = new TaskList();
        this.fileName = "." + File.separator + "profiles" + File.separator + name + ".json";;
        this.fileHandler = new JsonFileHandler(this.fileName);

        //if file with given name already exists update task list first
        if (fileExist(name)){
            updateTaskList();
            updateFile();
        }else{
            updateFile();
            updateTaskList();
        }
    }

    public TaskList getTaskList() {
        return taskList;
    }

    //adding new task to the list,
    void addTask(int priority, String content){
        updateTaskList();       //updating task list object from file
        this.taskList.addTask(priority, content);   //adding task to the list
        updateFile();           //updating task list file from object
    }

    //removing task from the list
    void removeTask(int id){
        updateTaskList();   //updating task list object from file
        taskList.removeTask(id);    //removing task from the list, that has certain ID
        updateFile();        //updating task list file from object
    }

    void printTaskList(){
        try {
            fileHandler.printContent();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    //updating task list object from file
    void updateTaskList(){
        try{
            taskList = fileHandler.fileToObj();
        } catch (IOException e){
            throw new RuntimeException(e);
        }
    }

    //updating task list file from object
    void updateFile(){
        try {
            fileHandler.objectToFile(this.taskList);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    //method chekcs if file with this name already exists
    boolean fileExist(String testName){
        File profilesDirectory = new File("profiles");

        File[] profileFiles = profilesDirectory.listFiles();
        if (profileFiles == null) {
            System.err.println("No files found in profiles directory.");
            return false;
        }

        for (File file : profileFiles) {
            if (file.isFile() && file.getName().endsWith(".json")) {
                String fileName = file.getName();
                // Extract name from filename (remove .json extension)
                String profileName = fileName.substring(0, fileName.lastIndexOf('.'));
                if(profileName.equals(testName)){return true;}
            }
        }
        return false;
    }
}
