//main application screen
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

public class MainScreen {
    Profile activeProfile;
    JFrame mainScreen;
    JPanel taskListDiv;
    JScrollPane taskListDivScrollPane;
    JLabel titleLabel;
    TaskCreatingDialog taskCreatingDialog;
    ProfileScreen profileScreen;
    MainScreen(){
        this.activeProfile = null;
        this.profileScreen = new ProfileScreen(this);
        profileScreen.displayProfileSelectionDialog();
    }

    //displaying main screen
    void displayMainScreen(){

        System.out.println("profil using mainScreen is " + activeProfile.name);
        mainScreen = new JFrame(this.activeProfile.name + "'s Task List");
        mainScreen.setLayout(new BorderLayout());

        //creating instance of task creating dialog
        taskCreatingDialog = new TaskCreatingDialog(this);
        //adding title text
        titleLabel = new JLabel("Hello " + activeProfile.name + "! Here are your tasklist!");
        mainScreen.add(titleLabel, BorderLayout.NORTH);

//        //task DivList, that will store all visible on the screen task components
//        List <TaskDiv> taskList = new ArrayList<>();


        taskListDiv = new JPanel();
        taskListDiv.setLayout(new BoxLayout(taskListDiv, BoxLayout.Y_AXIS)); // Set layout to vertical

        //updating and displaying task list
        addTaskListContent();


        JPanel controlButtonPanel = new JPanel();

        //create task button
        JButton createTaskButton = new JButton("Create new Task");
        createTaskButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                taskCreatingDialog.displayTaskCreatingDialog();
            }
        });

        //adding switch profile button
        JButton switchProfileButton = new JButton("Switch profile");
        switchProfileButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainScreen.setVisible(false);
                profileScreen.displayProfileSelectionDialog();

            }
        });

        controlButtonPanel.add(switchProfileButton);
        controlButtonPanel.add(createTaskButton);

        //adding control button panel to a main screen
        mainScreen.add(controlButtonPanel, BorderLayout.SOUTH);

        mainScreen.setSize(500, 600);
        mainScreen.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);      //exit when close button is clicked


        mainScreen.setVisible(true);

    }

    //method updating taks list on the main screen
    void updateTaskListDiv(){
        //clearing old scrollPane
        mainScreen.remove(taskListDivScrollPane);
        // Clear the task list div and re-add task components
        taskListDiv.removeAll();

        addTaskListContent();

        // Refresh the frame to reflect the updates
        mainScreen.revalidate();
        mainScreen.repaint();
    }

    //updating content of the task list and displaying it
    void addTaskListContent(){
        //sorting task list before displaying it
        activeProfile.getTaskList().sortTaskList();
        //adding task divs to a task div list
        for(Task task : activeProfile.getTaskList().taskList){
            TaskDiv taskDiv = new TaskDiv(this, task);
            //taskList.add(taskDiv);
            taskListDiv.add(taskDiv.getDiv());
        }

        //setting scrollpane preffered size
        // Set preferred size of taskListDiv based on combined preferred heights of child components
        int preferredHeight = 0;
        for (Component component : taskListDiv.getComponents()) {
            preferredHeight += component.getPreferredSize().height;
        }
        Dimension preferredSize = new Dimension(taskListDiv.getPreferredSize().width, preferredHeight);
        taskListDiv.setPreferredSize(preferredSize);

        //adding scrollPane so user can navigate over large number of tasks
        taskListDivScrollPane = new JScrollPane(taskListDiv);
        taskListDivScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        taskListDivScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        //setting scroll speed
        taskListDivScrollPane.getVerticalScrollBar().setUnitIncrement(16);
        taskListDivScrollPane.getVerticalScrollBar().setBlockIncrement(50);
        //adding scrollpane to a mainscreen
        mainScreen.add(taskListDivScrollPane, BorderLayout.CENTER);
    }


}
