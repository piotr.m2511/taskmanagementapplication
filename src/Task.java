//task object, containing its id, priority, status and task content.
public class Task {
    int id;
    int priority;
    String content;
    boolean isDone;

    public Task(int id, int priority, String content) {
        this.id = id;
        this.priority = priority;
        this.content = content;
        this.isDone = false;
        System.out.println("Task #" + this.id + " created!");
    }

    public void printTask() {
        System.out.println("####################\nTask #" + this.id + "\nPriority: " + this.priority + "\nContent: " + this.content + "\nis done: " + this.isDone);
    }

    public int getId() {
        return id;
    }

    public String getContent() {
        return content;
    }

    public boolean isDone() {
        return isDone;
    }

    public void setDone(boolean done) {
        isDone = done;
    }

    public int getPriority() {
        return priority;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
