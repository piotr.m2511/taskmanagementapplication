import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class JsonFileHandler {
    String fileName;    //path to the json file

    Gson gson = new GsonBuilder().setPrettyPrinting().create();
    String content;

    public JsonFileHandler(String fileName) {
        this.fileName = fileName;
    }

    //method saving task list object to the json file
    public void objectToFile(TaskList taskList) throws IOException {
        try(FileWriter writer = new FileWriter(fileName)){
            gson.toJson(taskList, writer);
        }
    }

    //method reading json file and returning its content as an object
    public TaskList fileToObj() throws IOException{
        try(FileReader reader = new FileReader(fileName)){
            return gson.fromJson(reader, TaskList.class);
        }
    }
    //printing content of JSON file on the console
    public void printContent () throws IOException {
        try(FileReader reader = new FileReader(fileName)){
            Object jsonContent = gson.fromJson(reader, Object.class);
            System.out.println(gson.toJson(jsonContent));
        }

    }

}
