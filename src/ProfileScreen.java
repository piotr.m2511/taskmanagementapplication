//clas containing profile selection screen

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.util.Vector;

public class ProfileScreen {
    String selectedProfile;
    JDialog profileScreen;
    JComboBox combobox;

    Vector<String> profileList;
    Profile activeProfile;
    MainScreen parent;

    ProfileScreen(MainScreen mainScreen){
        //initalizing mainFrame parent class
        this.parent = mainScreen;
        createProfilesDirectory();
    }

    //method displaying and adding elements
    void displayProfileSelectionDialog(){
        //preparing arraylist of profiles used in combobox and adding new profile selection
        profileList = getProfileNames();
        profileList.add("[NEW PROFILE]");

        //setting a Frame
        profileScreen = new JDialog(parent.mainScreen,"Profile selection");


        //adding title so user knows what to do
        JLabel titleLabel = new JLabel("Choose your profile!");

        //creating combobox storing profiles
        combobox = new JComboBox<>(profileList);

        //test label
        JLabel testLabel = new JLabel();

        //button executing select action
        JButton selectButton = new JButton("SELECT");

        selectButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                selectedProfile = (String) combobox.getSelectedItem();

                if (selectedProfile == "[NEW PROFILE]"){
                    createNewProfile();
                }else {
                    activeProfile = new Profile(selectedProfile);
                    parent.activeProfile = activeProfile;
                    profileScreen.setVisible(false);
                    parent.displayMainScreen();
                }

            }
        });

        //adding remove button
        JButton removeButton = new JButton("REMOVE");
        removeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                removeSelectedProfile();
            }
        });


        profileScreen.add(titleLabel);
        profileScreen.add(combobox);
        profileScreen.add(selectButton);
        profileScreen.add(removeButton);
        profileScreen.add(testLabel);

        profileScreen.setLayout(new FlowLayout(FlowLayout.LEADING));
        profileScreen.setSize(200,400);
        profileScreen.setVisible(true);

    }
    //method that reads files from the profile direction and creates a Vector from them.
    public static Vector<String> getProfileNames() {
        Vector<String> profileNames = new Vector<>();

        File profilesDirectory = new File("profiles");
        if (!profilesDirectory.exists() || !profilesDirectory.isDirectory()) {
            System.err.println("Profiles directory not found.");
            return profileNames;
        }

        File[] profileFiles = profilesDirectory.listFiles();
        if (profileFiles == null) {
            System.err.println("No files found in profiles directory.");
            return profileNames;
        }

        for (File file : profileFiles) {
            if (file.isFile() && file.getName().endsWith(".json")) {
                String fileName = file.getName();
                // Extract name from filename (remove .json extension)
                String profileName = fileName.substring(0, fileName.lastIndexOf('.'));
                profileNames.add(profileName);
            }
        }

        return profileNames;
    }
    //metgod updating profile list as well as combobox content, basing on

    void updateProfileList(){
        profileList = getProfileNames();
        profileList.add("[NEW PROFILE]");
        combobox.removeAllItems();
        for (String s : profileList){
            combobox.addItem(s);
        }
    }

    //method opening creating profile dialog
    void createNewProfile(){
        ProfileCreator creator = new ProfileCreator(this);
    }


    //method removin selected profile's file
    void removeSelectedProfile(){
        selectedProfile = (String) combobox.getSelectedItem();
        if (selectedProfile == null || selectedProfile.equals("[NEW PROFILE]")) {
            JOptionPane.showMessageDialog(profileScreen, "Please select a valid profile to remove.", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }
        int confirmation = JOptionPane.showConfirmDialog(profileScreen, "Are you sure you want to remove a " + selectedProfile +
                " profile and its data?", "Removel confirm", JOptionPane.YES_NO_OPTION);
        if (confirmation == JOptionPane.YES_OPTION){
            File profileFile = new File("profiles" + File.separator +selectedProfile + ".json");
            if (profileFile.exists()){
                if (profileFile.delete()){
                    JOptionPane.showMessageDialog(profileScreen, "Profile " + selectedProfile +
                            " deleted successfully!","Success", JOptionPane.INFORMATION_MESSAGE);
                    updateProfileList();
                }
                else {
                    JOptionPane.showMessageDialog(profileScreen, "Failed to remove file!", "Fail", JOptionPane.ERROR_MESSAGE);
                }
            }
            else{
                JOptionPane.showMessageDialog(profileScreen, "This profile's file could not be found!", "Fail", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    // Check if the "profiles" directory exists, and create it if it doesn't
    private void createProfilesDirectory() {
        File profilesDirectory = new File("profiles");
        if (!profilesDirectory.exists()) {
            if (profilesDirectory.mkdirs()) {
                System.out.println("Profiles directory created successfully.");
            } else {
                System.err.println("Failed to create profiles directory.");
            }
        }
    }
}
