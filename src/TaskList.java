import java.util.ArrayList;
import java.util.Comparator;

public class TaskList {
    public ArrayList<Task> taskList;

    int sequence;

    //konstrutkor ustalajacy liczb
    public TaskList() {
        this.taskList = new ArrayList<Task>();
        this.sequence = 1;
    }

    public void addTask(int priority, String content){
        this.taskList.add(new Task(sequence, priority, content));
        this.sequence ++;
    }

    public Task getTaskByID(int ID){
        for (Task t : taskList){
            if (t.getId() == ID){
                return t;
            }
        }
        return null;
    }
    public void removeTask(int id){
        System.out.println("Task #"+ getTaskByID(id).getId() + " deleted!");
        taskList.remove(getTaskByID(id));
    }
    public void printAllTasks(){
        for (Task t : taskList){
            t.printTask();
        }
    }
    public void sortTaskList(){
        taskList.sort(new Comparator<Task>() {
            @Override
            public int compare(Task t1, Task t2) {
                // First, compare by status (done tasks at the bottom)
                if (t1.isDone() && !t2.isDone()) {
                    return 1;
                } else if (!t1.isDone() && t2.isDone()) {
                    return -1;
                } else {
                    // If both tasks have the same status, compare by priority (higher priority first)
                    return Integer.compare(t2.getPriority(), t1.getPriority());
                }
            }
        });
    }
}

