import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

//single task object in gui. Label with task content and its buttons like "make done"
public class TaskDiv {
    MainScreen parent;  //parent frame
    Task task;     //related task
    JPanel div;
    ImageIcon tickIcon;
    TaskDiv(MainScreen parent, Task sourceTask){
        this.task = sourceTask;
        this.parent = parent;
        this.div = new JPanel();

        //adding priority Label
        String priorityText = "PRIORITY: " + this.task.getPriority();
        JLabel priorityLabel = new JLabel(priorityText);

        //content field with task content displayed
        JTextField contentTextField = new JTextField(sourceTask.getContent(), 20);  //here will be displayed Task content

        //if task is completed, then set textfield uneditable
        contentTextField.setEditable(!task.isDone());

        //auto updating task content, if user changes it
        contentTextField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                updateTaskContent();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                updateTaskContent();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                updateTaskContent();
            }

            private void updateTaskContent() {
                task.setContent(contentTextField.getText());
                parent.activeProfile.updateFile();  // Update JSON file whenever text changes
            }
        });
        //adding new icon which symbolises completing of a task
        tickIcon = new ImageIcon("." + File.separator + "src" + File.separator + "assets" + File.separator + "tickIcon.jpg");

        //completing task button. It makes task uneditable
        JButton completingButton = new JButton(tickIcon);

        //action for "DONE" button
        completingButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                task.setDone(!task.isDone());   //setting opposite of current task status
                parent.activeProfile.updateFile();  //updating JSON file
                contentTextField.setEditable(!task.isDone());   //visualisation of task status
                parent.updateTaskListDiv();
            }
        });

        
        //adding remove button
        JButton removeButton = new JButton("REMOVE");
        removeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //removing this task
                parent.activeProfile.taskList.removeTask(task.getId());
                parent.activeProfile.updateFile();  //updating JSON file
                parent.updateTaskListDiv();
            }
        });

        //adding button panel to a task div
        JPanel buttonPanel = new JPanel();
        buttonPanel.add(completingButton);
        buttonPanel.add(removeButton);

        //adding components to a panel
        div.add(contentTextField);
        div.add(priorityLabel);
        div.add(buttonPanel);
        div.setLayout((new FlowLayout(FlowLayout.LEFT)));
        div.setVisible(true);
    }

    public JPanel getDiv() {return div;}
}