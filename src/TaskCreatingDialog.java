import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

//this is dialog screen which enables adding new tasks to the list
public class TaskCreatingDialog {

    JDialog taskCreatingDialog;
    MainScreen parent;

    TaskCreatingDialog(MainScreen mainScreen){
        this.parent = mainScreen;

    }

    //method that adds components and displays it on the dialog
    void displayTaskCreatingDialog(){

        this.taskCreatingDialog = new JDialog(parent.mainScreen, "Adding new task");

        //content panel including text field and priority
        JPanel contentPanel = new JPanel();

        //taskContent text field
        TextField contentTF = new TextField("Enter your task");

        contentPanel.add(contentTF);

        JLabel priorityLabel = new JLabel("PRIORITY:");
        contentPanel.add(priorityLabel);
        //adding a spinner, which can adjust task priority
        JSpinner prioritySpinner = new JSpinner(new SpinnerNumberModel(1, 1, 5, 1));


        contentPanel.add(prioritySpinner);
        contentPanel.setLayout(new FlowLayout());
        //button panel containing CREATE and CANCEL
        JPanel buttonPanel = new JPanel();


        //create button
        JButton createButton = new JButton("CREATE");
        createButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                parent.activeProfile.taskList.addTask((Integer) prioritySpinner.getValue(), contentTF.getText());
                parent.updateTaskListDiv();

            }
        });
        buttonPanel.add(createButton);
        //adding cancel button
        JButton cancelButton = new JButton("CANCEL");
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                taskCreatingDialog.setVisible(false);
            }
        });
        buttonPanel.add(cancelButton);

        //adding components to a dialog
        taskCreatingDialog.setSize(500, 200);
        taskCreatingDialog.setLayout(new FlowLayout());
        taskCreatingDialog.add(contentPanel);
        taskCreatingDialog.add(buttonPanel);

        taskCreatingDialog.setVisible(true);
    }
}